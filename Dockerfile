FROM node:16

WORKDIR /srv/socket

COPY ./package.json ./
COPY ./yarn.lock ./ 

RUN npm install

COPY . .

RUN npm run build

EXPOSE 4000

CMD ["npm", "run", "start"]
