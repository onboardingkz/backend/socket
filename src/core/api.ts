import axios from "axios"

export const api = axios.create({
    baseURL: process.env.API_URL,
});

export const apiWithAuth = (token: string) => axios.create({
    baseURL: process.env.API_URL,
    headers: {
        Authorization: `Bearer ${token}`
    }
});