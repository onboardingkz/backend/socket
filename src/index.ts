import dotenv from "dotenv";
import express from "express";

import { createServer } from "http";
import { Server } from "socket.io";
import cors from 'cors';
import { api, apiWithAuth } from "./core";
import figlet from "figlet";

// initialize configuration
dotenv.config();

// port is now available to the Node.js runtime
// as if it were an environment variable
const port = process.env.SERVER_PORT;
const app = express();

app.use(cors());

const http = createServer(app);

const io = new Server(http, {
    cors: {
        origin: "*",
        methods: ['GET', 'POST']
    }
});

const sessions: Record<string, Record<string, string>> = {};

io.on("connection", (socket) => {
    console.log(`User connected with id ${socket.id}`);
    socket.on('auth', ({ token, userId }) => {
    	console.log(token, userId);
	    sessions[userId] = { socketSession: socket.id, token };
    });

    socket.on('NOTIFY', ({ userId, notification }) => {
        io.to(sessions[userId]?.socketSession).emit("NOTIFY", notification);
    });

    socket.on('CLEAR', ({ userId }) => {
        io.to(sessions[userId]?.socketSession).emit("CLEAR");
    });

    socket.on("disconnect", () => {
        Object.entries(sessions).map(([key, value]) => {
            if (value.socketSession === socket.id) {
                delete sessions[key];
            }
        });
    });
});

http.listen( port, () => {
    // tslint:disable-next-line:no-console
    figlet("OnBoarding RTC", (err, data) => {
        if (err === null) {
            console.log("_____________");
            console.log("\x1b[32m%s\x1b[0m", data);
            console.log("\x1b[1m%s\x1b[0m", `\x1b[32m✔\x1b[0m Socket server listening at http://localhost:${ port }`);
        }
    });
} );
